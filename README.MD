# TASK MANAGER

## DEVELOPER INFO

**NAME**: Anastasiya Aksenova

**EMAIL**: aaksenova@t1-consulting.ru

**EMAIL**: cs.aksenova@gmail.com

## SOFTWARE

**JAVA**: OPENJDK 1.8

**OS**: Microsoft Windows 10 Home

## HARDWARE

**CPU**: i7

**RAM**: 16GB

**SSD**: 111GB

## BUILD PROGRAM

```
mvn clean install
```

## RUN PROGRAM

```
java -jar ./task-manager.jar
```
