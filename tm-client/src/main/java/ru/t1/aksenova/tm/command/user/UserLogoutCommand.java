package ru.t1.aksenova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.request.UserLogoutRequest;
import ru.t1.aksenova.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "logout";

    @NotNull
    public static final String DESCRIPTION = "Logout current user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        @Nullable final UserLogoutRequest request = new UserLogoutRequest(getToken());
        getAuthEndpointClient().logout(request);
        setToken(null);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
