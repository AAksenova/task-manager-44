package ru.t1.aksenova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.model.IWBS;
import ru.t1.aksenova.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public class Project extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(length = 100, nullable = false)
    private String name = "";

    @NotNull
    @Column(length = 255)
    private String description = "";

    @NotNull
    @Column(length = 30)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(updatable = false, nullable = false)
    private Date created = new Date();

    @NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    public Project(
            @NotNull final String name,
            @NotNull final Status status
    ) {
        this.name = name;
        this.status = status;
    }

}
