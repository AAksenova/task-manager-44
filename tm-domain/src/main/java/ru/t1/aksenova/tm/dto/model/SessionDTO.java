package ru.t1.aksenova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_session")
public final class SessionDTO extends AbstractUserOwnedModelDTO {

    private static final long serialVersionUID = 1;

    @Column
    @NotNull
    private Date date = new Date();

    @Nullable
    @Column(length = 50)
    @Enumerated(EnumType.STRING)
    private Role role = null;

}
