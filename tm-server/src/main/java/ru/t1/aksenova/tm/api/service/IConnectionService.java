package ru.t1.aksenova.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface IConnectionService {

    @NotNull
    @SneakyThrows
    EntityManager getEntityManager();
}
