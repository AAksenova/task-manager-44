package ru.t1.aksenova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.aksenova.tm.dto.model.TaskDTO;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.enumerated.TaskSort;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskDTOService extends IUserOwnedDTOService<TaskDTO> {

    @NotNull ITaskDTORepository getRepository(@NotNull EntityManager entityManager);

    @NotNull TaskDTO create(@Nullable TaskDTO task);

    @NotNull TaskDTO create(
            @Nullable String userId,
            @Nullable TaskDTO task
    );

    @NotNull
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull Collection<TaskDTO> set(@NotNull Collection<TaskDTO> tasks);

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId);

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId, @Nullable TaskSort sort);

    @NotNull
    List<TaskDTO> findAll(
            @Nullable String userId,
            @Nullable Comparator<TaskDTO> comparator
    );

    @NotNull
    List<TaskDTO> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

    @Nullable
    TaskDTO findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    void clear();

    void removeAll(@Nullable String userId);

    @Nullable
    TaskDTO removeOne(
            @Nullable String userId,
            @Nullable TaskDTO task
    );

    @Nullable
    TaskDTO removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    TaskDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull TaskDTO changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    boolean existsById(@Nullable String userId, @Nullable String id);

    long getSize(@Nullable String userId);

}
