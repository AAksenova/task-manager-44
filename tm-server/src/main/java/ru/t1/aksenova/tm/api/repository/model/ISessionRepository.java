package ru.t1.aksenova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.model.Session;
import ru.t1.aksenova.tm.model.Task;

import java.util.List;

public interface ISessionRepository extends IUserOwnedRepository<Session>{

    @NotNull
    List<Session> findAll();

    @NotNull
    List<Session> findAll(@NotNull String userId);

    @Nullable
    Session findOneById(@NotNull String id);

    @Nullable
    Session findOneById(@NotNull String id, @NotNull String userId);

    void removeById(@NotNull String id);

    void removeById(@NotNull String id, @NotNull String userId);

    void clear();

    void clear(@NotNull String userId);

    long getCount();

    long getCount(@NotNull String userId);

    boolean existById(@NotNull String id);

    boolean existById(@NotNull String id, @NotNull String userId);

}
