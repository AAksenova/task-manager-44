package ru.t1.aksenova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.model.ProjectDTO;
import ru.t1.aksenova.tm.enumerated.ProjectSort;

import java.util.Comparator;
import java.util.List;

public interface IProjectDTORepository extends IUserOwnedDTORepository<ProjectDTO> {

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAll(@NotNull String userId);

    @NotNull
    List<ProjectDTO> findAll(@NotNull String userId, @NotNull ProjectSort sort);

    @NotNull
    List<ProjectDTO> findAll(@NotNull String userId, @NotNull Comparator<ProjectDTO> comparator);

    @Nullable
    ProjectDTO findOneById(@NotNull String id);

    @Nullable
    ProjectDTO findOneById(@NotNull String id, @NotNull String userId);

    void removeById(@NotNull String id);

    void removeById(@NotNull String id, @NotNull String userId);

    void clear();

    void clear(@NotNull String userId);

    long getCount();

    long getCount(@NotNull String userId);

    boolean existById(@NotNull String id);

    boolean existById(@NotNull String id, @NotNull String userId);

}
